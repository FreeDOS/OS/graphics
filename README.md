# Graphics

Graphics related to FreeDOS and the repositories

# Logos
![logo4](freedos-logo4.svg)
![logo3](freedos-logo3.png)
![logo2](freedos-logo2-lg.png)
![logo1](freedos-logo-orig3.svg)

# Blinky the Mascot
![color-svg](fdfish-color-plain.svg)
![glossy-svg](fdfish-glossy-plain.svg)
![bw-text-svg](fdfish-bw-text.svg)
![bw-svg](fdfish-bw-plain.svg)

# Releases
![freedos-v1.3](FreeDOS-v1.3.bmp)

# Miscellaneous
![fdfloppy](fdfloppy.png)

